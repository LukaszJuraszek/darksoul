<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Item
 *
 * @ORM\Table(name="item")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ItemRepository")
 */
class Item
{
	public static $requirementsKeys = [
		'level',
		'strength',
		'speed'
	];
	
	public static $propertiesKeys = [
		'level',
		'strength',
		'speed'
	];
	
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @ORM\Column(type="string")
	 */
	protected $name;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $value;
	
	/**
	 * @ORM\Column(type="json_array")
	 */
	protected $requirements;
	
	/**
	 * @ORM\Column(type="json_array")
	 */
	protected $properties;
	
	/**
	 * @ORM\Column(type="json_array")
	 */
	protected $zones;
	
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Item
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value
     *
     * @param int $value
     * @return Item
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set requirements
     *
     * @param array $requirements
     * @return Item
     */
    public function setRequirements($requirements)
    {
        $this->requirements = $requirements;

        return $this;
    }

    /**
     * Get requirements
     *
     * @return array 
     */
    public function getRequirements()
    {
        return $this->requirements;
    }

    /**
     * Set properties
     *
     * @param array $properties
     * @return Item
     */
    public function setProperties($properties)
    {
        $this->properties = $properties;

        return $this;
    }

    /**
     * Get properties
     *
     * @return array 
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * Set zones
     *
     * @param array $zones
     * @return Item
     */
    public function setZones($zones)
    {
        $this->zones = $zones;

        return $this;
    }

    /**
     * Get zones
     *
     * @return array 
     */
    public function getZones()
    {
        return $this->zones;
    }
}
