<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Item;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Nelmio\Alice\Fixtures;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadItemsData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
	private $faker;
	
	/**
	 * @var ContainerInterface
	 */
	private $container;
	
	public function __construct()
	{
		$this->faker = Factory::create();
	}
	
	/**
	 * @param ContainerInterface|null $container
	 */
	public function setContainer(ContainerInterface $container = null)
	{
		$this->container = $container;
	}
	
	/**
	 * @param ObjectManager $manager
	 */
	public function load(ObjectManager $manager)
	{
		Fixtures::load(__DIR__ . '/Fixtures/items.yml', $manager, ['providers' => [$this]]);
	}
	
	public function requirements()
	{
		return [
			Item::$requirementsKeys[0] => $this->faker->numberBetween(1, 10),
			Item::$requirementsKeys[1] => $this->faker->numberBetween(1, 10),
			Item::$requirementsKeys[2] => $this->faker->numberBetween(1, 10),
		];
	}
	
	public function properties()
	{
		return [
			Item::$propertiesKeys[0] => $this->faker->numberBetween(1, 10),
			Item::$propertiesKeys[1] => $this->faker->numberBetween(1, 10),
			Item::$propertiesKeys[2] => $this->faker->numberBetween(1, 10),
		];
	}
	
	public function zones()
	{
		return [1, 2, 3];
	}
}
